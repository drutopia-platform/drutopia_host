# Introduction #

This Ansible script package can be used for managing Drutopia distributions on a server (or set of servers) that each host multiple members (commonly termed 'users'). In current lingo of this project, a member is ultimately an individual Drupal site installation (home folder, drupal source, public files, and database).

This system is intended to provide base distributions of Drupal that are shared by multiple members. Each member's site can be configured independently - this system will pull in relevant parts of those sites, limited to the configuration hierarchy, and custom themes.

**WARNING**: While the core distribution is controlled by the site administrator, it should be noted that themes are not (yet) fully sanitized to ensure no malicious custom PHP is contained within them. An administrator of a Drutopia server should ensure that if they allow member repositories to contain themes (which are automatically incorporated into their site) that the member's theme code is safe to include.

# Requirements #

In order to run provisioning scripts, you must have the following installed:

 - [Ansible](https://docs.ansible.com/#project) v2.4 or higher
 - A [SASS](https://sass-lang.com/) compiler, if necessary per your themes in use. The C implementation of SASS (`sassc`) available natively on many systems is used by default. Any other may be specified with the `sass_compiler` variable.
 - PHP [composer](https://getcomposer.org/), used to build your Drupal releases.

# Conceptual Overview #

Starting from a base installation of Debian 9, the core playbook `provision.yml` will configure all software required to run multiple instances of Drupal sites. This includes installing and configuring underlying services, such as Apache and MariaDB. At present, Drutopia expects a single system provides all roles (web, database, etc.). Once the core system is configured (tasks involved in this process are tagged with `setup`), the provision script moves on to build and deployment tasks.

The build and push - which is distinct from deploy - tasks create a build of a Drupal system based on Composer files, and then push them to a shared repository of codebases on the Drutopia server.

Finally, site deployment takes place. For each member site listed, the system will pull the appropriate version from the shared repository into the member folder and configure it. This includes an initial site install, or when a site is already present, the database update, config import, and other related steps.

# Using Drutopia Host #

## Configuration ##

As this system is based on Ansible, a working knowledge of Ansible is required. We will not cover all possible variables utilized throughout the project. Understanding which to use, and where to place it requires knowledge of how best to tell Ansible what you are trying to accomplish. This project includes sample inventory, host and group vars in the appropriate folders that provide a reasonable example. The rudimentary configuration necessary involves configuring at least one inventory file, and the corresponding variables files (`group_vars` and `host_vars`). The playbook currently assumes a single host group name of `drutopia`.

### Inventory ###

Your inventory file must contain a drutopia group, with at least one host listed. This is the target you will deploy the drutopia hosting package to, including the listed sites. In it's simplest form, it might look like this:

```
[drutopia]
drutopia.example.com
```

### Variables ###

Aside from providing your general system's settings, a couple of core dictionaries configure your available builds and the sites that use them.

### Required settings ###

member_root: defaults to /home, as each site corresponds to a system user.
php_versions_enabled: present with defaults, but makes the most sense to define at the top-level.

##### drutopia_versions #####

The first is called `drutopia_versions`, and it contains at least one named release that is to be built and pushed to the server. This is not an active site - it is a combination of files comprising the core of the Drupal system, and related files. These will be referenced by actual sites later.

```
drutopia_versions:
  stable:
    git_repo: git@gitlab.com:drutopia/drutopia_template.git
    git_version: master
    use_sass: false
    active: true
```

The above entry defines a release that on the server will be known as "stable". This will be pulled to the machine running Ansible, built, and then pushed to the server (by default, within `/var/local/drutopia/stable`). Note that setting `active: false` controls the build/push process and does NOT remove any existing release. Anything flagged as inactive will not be being built, and/or pushed to the server. Any existing, previously pushed builds, will remain on the server. Repeated builds with the same key (in this example `stable`) will replace prior builds of the same name.

##### Setting up new PHP versions #####

When adding a new version of PHP, and not simultaneously adding a new site, you can set `php_remove_default_pool: false` in order to allow the php-fpm process to start. Otherwise, the handler will error out when it finds no pools available. After a run adding any site using that php version, set the value back to true to clean up the unused pool.

##### members #####

See `roles/members/README.md` for full details.

`members` is the other core dictionary which contains a list of each individual member site, and its associated settings.

Per-member options include Drupal install-time options, site settings, such as domain names, and also may include per-site configuration. Configuration for any particular site may (in most uses, should!) also live in a separate repository, specified by config_repo and config_version, in which case, custom themes support is also available.

An example members dictionary, as expected by the default `provision.yml`:

```yaml
---
members:
  # A unique site key, used as the username/dbname/etc..
  drutopia_org:
    # Online == false -> no site (offline result page in the near future).
    online: true

    # MariaDB settings:
    db_name: drutopia_org #not required: defaults to match site key above
    db_pwd: a_password
    db_user: drutopia_org #not required: defaults to match site key above

    # Drupal-specific settings (used during install)
    # Also see Apache settings below
    drupal_site_name: Drutopia
    drupal_email: admin@drutopia.org
    drupal_account_name: submin
    drupal_account_pass: NotARealPassword

    # Reference a key from drutopia_versions:
    drutopia_version: stable

    # Site customization
    custom_repo: git@example.com:our-private/drutopia-config.git
    custom_version: HEAD

    # If preferred, a local path may contain config (does not support themes):
    custom_path:

    # Perform config import during updates?
    config_import: true

    # Apache settings
    server_aliases: [www.drutopia.example.org]
    server_hostname: drutopia.example.org

    # Other system settings:
    fpm_slowlog: true

    # User access to this site:
    ssh_authorized_keys:
      - https://gitlab.com/(your user name here).keys
      - ssh-rsa AAAAB3NzaC1yc2EABBBADAQABAAA...etc
```

The `config_*` values of these specify where to pull customizations for the particular member. The drutopia_version should correspond to a listed build from drutopia_versions.

Drutopia Hosting provides a `members_file` variable, enabling another option for specifying the location of a list of members. This may be useful is an external system is used to record the list of active sites on the system. When this is set, the reference yml file is loaded via an `include_vars` task, which is expected to contain only the `members` variable.

### Initial server setup ###

A first-time provisioning does not technically require a members, nor drutopia_versions variable to be completed, but merely the top-level settings you wish to customize. When the basic configuration is completed, you can run the provision.yml to set up your server:

```
ansible-playbook -i inventory_sample provision.yml
```

Running the above command will also configure members and available drutopia versions, once they are configured, but these can be run selectively as well by using tags - see the next section.

### Limiting the playbook ###

As a single playbook handles all duties for the Drutopia hosting platform, it can be beneficial at times to run only certain pieces of a playbook. For instance, once your server has had it's initial setup completed, you'll likely not want to repeat these steps unless something has changed.

The playbook is partitioned internally by tags to handle this possibility. Tags can be used to exclude certain tasks/roles (by using `--exclude-tags`) or else can be used to run only certain tasks/roles, by using `tags`. Multiple tags can be specified for either argument to `ansible-playbook` by separating them with a comma.

#### Builds ####

To only build new versions and get them onto your server, use `build` and `push` tags like so:
```
ansible-playbook -i inventory_sample provision.yml --tags=build,push
```

#### [Individual] Site deploys ####

To run updates for the server's members only, use the `members` tag (note that any referenced `drutopia_version`s must have been pushed previously!). You may provision all member sites at once, or just a single instance, as in the following, where we only update the `test_member`'s new configuration and customizations using the extra-vars argument (`-e`) to set a `target_member`:

```
ansible-playbook -i inventory_sample provision.yml --tags=members -e "target_member=test_member"
```

#### Specifying versions ####

The version specified for either a drutopia_versions version, or a member's config_version may be blank (which equates to HEAD) the literal string HEAD, a branch name (master is the default), or a tag name. It may also be a specific SHA-1 hash, in which case config_refspec (not yet implemented) needs to be specified if the given revision is not already available.

#### Configuration and Themes ####

Two options exist for configuration management: using a git repository containing the configuration and/or themes, and using a local folder, which supports config only.

When using Git as a source of customization, the `git_ssh_key` variable must be set to a private key that will be used to fetch sources. The corresponding public key must have been added to your repository with read-only access. Within github, the public key can be added as a "Deployment key" in your project settings.

#### SSH key management ####

Individual sites each get a user account on the system. The ssh-keys able to access the system as that user are specified directly or via an URL reference.

#### Implementing SSL ####

If you have a wildcard certificate for your domain, and additional sites are hosted at it's sub-domains, the following SSL variables will configure your domains:

* `ssl_private_key`: private key contents
* `ssl_certificate`: contents of the certificate file, including cert chain
* `ssl_cert_path`: path to store the certificate on the server
* `ssl_key_path`: path to store the private key on the server

Certificates can be specified for specific sites. Per member entry, specify the following:
* site_ssl_cert_path: path to the certificate file on the server.
* site_ssl_key_path: path to the private key on the server.

This can be used along with certbot, which currently must be managed independently. Create your site first without specifying these values, then run certbot manually on the server to create the certificates. Following this, set the above properties, and run a minimal deploy to update the apache configuration.

The geerlingguy-certbot role may also be used to configure certificates. Example variables for this role:

certbot_auto_renew_user: root
certbot_auto_renew: true
certbot_auto_renew_minute: 28
certbot_auto_renew_hour: 1
certbot_create_if_missing: true
certbot_create_always: false
certbot_admin_email: webmaster@drutopia.org
certbot_create_standalone_stop_services:
  - apache2
certbot_certs:
  - domains:
    - "{{ ansible_host }}"
certbot_install_from_source: true
certbot_repo: https://github.com/certbot/certbot.git
certbot_version: master
certbot_keep_updated: true

# Directory structures #

## System-wide ##

The primary folder for organizing Drupal releases is controlled by `drutopia_library_path`, which will contain one folder for each key in the `drutopia_versions`. Below assumes there is a version called stable:

```
/var/local/drutopia/stable/
  web/
  vendor/
  VERSION (the full github hash of the build)
  activate.sh (run from a site context to make this version live)
```

## Per-site ##

Per-site organization is as follows:

```
<site root>/
  .drutopia/
    <site id>-fpm-pool.conf (linked from fpm pool.d when site online)
    <site id>-vhost.conf (linked from apache sites-enabled when site online)
  site/
    web/
      sites/default/files -> symlink to <site root>/files
    vendor/
  custom/
    config/
      sync/
        (configuration data)
  files/
```

Currently it is still necessary to manually set up `drush/sites/live.site.yml` and `drush/sites/test.site.yml` files.

# Local development #

Use the following directions in order to bring up a local test machine that you can target with Ansible to install Drutopia releases locally. Note that actual sites require individual host name configuration, but otherwise everything works via Ansible.

Limitations: 
- Unable to use certbot without a public IP and properly configured DNS.

- Configure the `test/inventory` and `test/test.yml` to your liking and run `ansible-playbook -i test/inventory test/test.yml all`

# Roadmap #

## Near-term ##

* Better role encapsulation, renaming to drutopia to "drutopia.build" and members to "drutopia.site".
* Integrate other role sources - specifically many of those published by geerlingguy on GitHub.
