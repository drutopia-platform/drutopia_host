---
- name: cache rebuild, because it seems always needed for drush to behave
  command: "{{ drush_path }} --root={{ drupal_dir }} cr"
  args:
    chdir: "{{ drupal_dir }}"
  changed_when: false
  ignore_errors: true

- name: check if site is installed
  command: "{{ drush_path }} --root={{ drupal_dir }} status --format=string --fields=bootstrap"
  args:
    chdir: "{{ drupal_dir }}"
  register: drupal_site_installed
  failed_when: drupal_site_installed.stdout is undefined or drupal_site_installed.rc != 0
  changed_when: false
  become: true
  become_user: "{{ drutopian }}"
  tags: drupal,members

- name: run backup tasks when site found
  block:
    # Note that if no config exists for user, this will run with just:
    # Configuration successfully exported [to ...]
    # all output from drush cex appears to go to stderr, not stdout
    - name: check for configuration changes
      shell: "{{ drush_path }} cex --no"
      args:
        chdir: "{{ drupal_dir }}"
      register: config_check
      failed_when: config_check.stderr is undefined or config_check.stderr == ''

    - name: determine configuration (mis)match
      set_fact:
        config_mismatch: "{{ 'active configuration is identical' not in config_check.stderr }}"
        config_new: "{{ 'Configuration successfully exported' in config_check.stderr }}"
        config_force: "{{ config_import_force_all | default(config_import_force) | bool or config_import_force }}"

    - name: archive existing configuration
      archive:
        dest: "{{ backup_dir }}/{{ backup_date }}-config{{ config_new | ternary('-new', '') }}.tar.gz"
        format: gz
        group: "{{ drutopian }}"
        owner: "{{ drutopian }}"
        mode: 0770
        path: "{{ drupal_dir }}/{{ config_sync_path }}/*"

    - name: check configuration status and create archives
      block:
        - name: fail deployment (unless forcing override)
          fail:
            msg: >
              "The configuration on the server {{ config_new |
              ternary('was not already present. Running again will complete, but stopping now since you should check this.',
              'has changed from the last-deployed configuration. Force all sites to deploy with -e \"config_import_force_all=true\" (use a space between existing -e instructions), or specify config_import_force for this site in site settings') }}."
          when: config_mismatch and not config_force

        - name: export updated configuration
          shell: "{{ drush_path }} cex -y"
          args:
            chdir: "{{ drupal_dir }}"
          register: config_backup
          failed_when: config_backup.stderr is undefined or 'Configuration successfully exported' not in config_backup.stderr
          when: config_mismatch and not config_new

        - name: archive modified configuration
          archive:
            dest: "{{ backup_dir }}/{{ backup_date }}-config-modified.tar.gz"
            format: gz
            group: "{{ drutopian }}"
            owner: "{{ drutopian }}"
            mode: 0770
            path: "{{ drupal_dir }}/{{ config_sync_path }}/*"
          when: config_mismatch and not config_new
      when: "not config_import_disable | default(False) | bool"

    - name: back up database
      community.mysql.mysql_db:
        name: "{{ db_name }}"
        state: dump
        target: "{{ backup_dir }}/{{ backup_date }}-database.sql.gz"
  when: "'Successful' in drupal_site_installed.stdout"
  become: true
  become_user: "{{ drutopian }}"
  rescue:
    - name: skip remaining tasks for member
      set_fact:
        skip_member: true
  tags: drupal,members
