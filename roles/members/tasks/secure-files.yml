---
- name: ensure web folder permissions are correct
  ansible.builtin.file:
    path: "{{ drupal_dir }}"
    mode: u=rwX,g=rX,o=rX
    owner: "{{ user_managed_site | ternary(drutopian, 'root') }}"
    group: "{{ drutopian }}"
    recurse: true
    follow: false

- name: locate all settings files
  ansible.builtin.find:
    path: "{{ drupal_dir }}/sites/default"
    patterns: "settings*php"
  register: settings_files

- name: ensure settings permissions are correct
  ansible.builtin.file:
    path: "{{ settings_file.path }}"
    mode: u=r,g=r,o=
    owner: "{{ user_managed_site | ternary(drutopian, 'root') }}"
    group: "{{ drutopian }}"
  loop: "{{ settings_files.files }}"
  loop_control:
    loop_var: settings_file
  when: not settings_files.failed

- name: ensure config folder permissions are correct
  ansible.builtin.file:
    path: "{{ drupal_dir }}/{{ config_sync_path }}"
    recurse: true
    mode: u=rwX,g=rwX,o=
    owner: "{{ drutopian }}"
    group: "{{ drutopian }}"

- name: ensure custom folder permissions are correct
  ansible.builtin.file:
    path: "{{ custom_dir }}"
    recurse: true
    mode: u=rwX,g=rwX,o=
    owner: "{{ drutopian }}"
    group: "{{ drutopian }}"

- name: ensure vendor folder permissions are correct
  ansible.builtin.file:
    path: "{{ site_dir }}/vendor"
    recurse: true
    mode: u=rwX,g=rX,o=
    owner: "{{ user_managed_site | ternary(drutopian, 'root') }}"
    group: "{{ drutopian }}"

- name: ensure composer permissions are correct
  ansible.builtin.file:
    path: "{{ composer_file }}"
    mode: u=rw,g=r,o=
    owner: "{{ user_managed_site | ternary(drutopian, 'root') }}"
    group: "{{ drutopian }}"
  loop:
    - '{{ site_dir }}/composer.json'
    - '{{ site_dir }}/composer.lock'
  loop_control:
    loop_var: composer_file

- name: locate all vendor/bin files
  ansible.builtin.find:
    path: "{{ site_dir }}/vendor/bin"
    file_type: any
  register: bin_files

- name: ensure bin files are executable
  ansible.builtin.file:
    path: "{{ bin_file.path }}"
    mode: u+x,g+x
  loop: "{{ bin_files.files }}"
  loop_control:
    loop_var: bin_file
  when: not bin_files.failed
